![alt text](BSF-logo.png)

# Introduction

If you have any questions related to bodybuilding/fitness be sure to check out this wiki first.
When you can't find more specific information on the Git, you can always ask your question in the server directly, we are happy to help.

- [x] [BSF on Discord](https://discord.gg/atpCszx)
- [x] [Rate our server](https://top.gg/servers/688710650507165697)

# Beginner guide
- [x] [Beginner basics](beginner/beginner-basics.md)
- [x] [Get a small home gym started](resistance-training/buying_equipment.md)
- [ ] [What food should I eat?](nutrition/what-to-eat.md)
- [ ] [Should I do fasting, keto or vegan diets?](nutrition/fat-diets.md)

# Mindset & Motivation

- [ ] [Lifting won't fix your life](https://youtu.be/bhoWS1l1tFk?t=588)
- [ ] [Motivation, discipline and dedication](mindset-and-motivation/Motivation-disipline-dedication.md)
- [ ] [Body dysmorphia and preventing eating disorders](mindset-and-motivation/disorders.md)
- [ ] [I skipped a workout and now I feel bad](mindset-and-motivation/skipping-workouts.md)
- [ ] [Should I get a training buddy? Independence and consistency](blabla)

<!------------------------------------------------------------------>

# Stretching and posture

- [ ] [Beginner stretching](https://www.youtube.com/watch?v=Zq_XwV_ZpsM)

# Sleep

- [ ] [Sleep basics](sleep/sleep.md)
- [ ] [Snoring and Sleep Apnea](https://www.heart.org/en/health-topics/sleep-disorders/sleep-apnea-and-heart-disease-stroke)

<!------------------------------------------------------------------>

# Cardio

* [ ] [Cardio basics](cardio/cardio-basics.md)

# Resistance Training

* [ ] [Create your own routine](resistance-training/create-routine.md)
* [ ] [Exercise library](resistance-training/excersice-library.md)

# Tools

* https://bodywhat.com/
* https://ffmicalculator.org/
* https://www.calculator.net/bmi-calculator
* https://symmetricstrength.com/


# Nutrition

* [ ] [Workout nutrition](workout_nutrition.md)
* [ ] [Creatine](nutrition/creatine_supplementation.md)

<!------------------------------------------------------------------>

# Recommended external blogs/channels

Bodybuilding/Fitness:
* [IFBB PRO JAMES HOLLINGSHEAD](https://www.youtube.com/channel/UCD8wepfBT-yDt4pmI8-z5bg)
* [Thomas Maw](https://www.youtube.com/c/TMCycles)
* [A.J Morris](https://www.youtube.com/channel/UCz6Mly4rhmeML0zDjo6yDrg)
* [Cameron Mackay](https://www.youtube.com/channel/UCo3dKndpV6HMUU1z5IQ-fTw)
* [Finn Kelly](https://www.youtube.com/channel/UCr8hEuU76Ww5JvlFzpW4dDQ)
* [Josh Bridgman](https://www.youtube.com/channel/UCg_1gRpsCuq-s-w54UqANfA)
* [KHIFIE WEST](https://www.youtube.com/channel/UCA4KRYGKV9Me7qxRaVXfVww)
* [Kuba Cielen](https://www.youtube.com/channel/UCIfNocTBJECCId_oW11YeRw)
* [Scooby werkstat](https://www.youtube.com/user/VitruvianPhysique)
* [Mark Coles M10](https://www.youtube.com/channel/UCZ7yH-kLdFj7HNo2Dcs6FnQ)
* [UK PRO MUSCLE PODCAST](https://www.youtube.com/channel/UC7cZErjKpG3xX7QrSkpC6aw)
* [The Bioneer](https://www.youtube.com/channel/UCIh_TPYPqjJuS_-nOfAIlfg)
* [Shredded Sports Science](https://www.youtube.com/channel/UCXrqErU_TjqiHAHJkzITAvg)

Cooking and nutrition:
* [Ethan Chlebowski](https://www.youtube.com/channel/UCDq5v10l4wkV5-ZBIJJFbzQ)

Strength training:
* [Alan Thrall](https://www.youtube.com/channel/UCRLOLGZl3-QTaJfLmAKgoAw)
* [Alexander Bromley](https://www.youtube.com/channel/UCclRFAVSUGe8cu9JkvY6nNA)

Athletic training:
* [Athlete x](https://www.youtube.com/channel/UC-wepCJBjdSNQnFL6UOAHvg)
* [Mathias Hove Johansen](https://www.youtube.com/channel/UCWoBbgNqAVVke2Ny9pqAeuQ)
* [Flow High Performance](https://www.youtube.com/channel/UCSiKFGDpfCICQDfmxxjDvsg)
* [Overtimeathletes](https://www.youtube.com/channel/UCM4GEVa1Qi1CTdJg0ljdoow)
