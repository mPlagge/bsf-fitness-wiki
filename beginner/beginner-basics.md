# Beginner basics, Get your ass moving!

We have a lot of information written about nutrition, motivation/mindset, hypertrophy and more important information you need to build a good base knowledge about bodybuilding and fitness. It is recommended to read up on more subjects after you've chosen a basic nutrition and training program.

## I'm too skinny

Nutrition is very important to  grow. But first you will need a training program that fits your needs. You want to gain muscle not fat. Choose one in the training program section.

To keep things short here about nutrition, pick high calorie dense foods, make shakes to drink your calories, drink juices, add heavy cream to your shakes. Add healthy oils to your food or shot them (coconut oil/olive oil). A more complete guide about bulking up can be found in  [Bulking in detail](nutrition/bulking.md).


## I'm too fat

Go into a caloric deficit... it is that easy. Keep track of your weight on the scale, count your calories, and if it isn't moving down after a week of deficit lower calories a bit more and repeat. A good rule of thumb is to lose about half a kilo per week.

Don't cook in oils, boil or use non-stick sprays and don't drink your calories, like fruit drinks and shakes. Drink a lot of water and eat low calorie dense foods. This might help you to already some lose weight. For more tricks and techniques to cut down in calories see: [Cutting in detail](nutrition/cutting.md).

It it also recommended to do cardio and resistance training for general health and aesthetics. Also, more muscle will burn more calories, while cardio training itself burns calories. Both can be a useful tool for weight loss.

## I'm not (sure) if I'm skinny or fat, should I cut or bulk?

If you are not happy about your bodyfat you should work on that first, and read the *I'm too fat* guide. If you like the amount of fat on your body start with the *I'm too skinny* guide.

## Training splits

Pick a routine that will suit your schedule. If you're a busy person a full body split may be good for you. However, if you have lots of time on your hands you may want to do a upper lower split. A full body split would be training your whole body in each session (i.e chest, back, legs and arms). You train this x3 a week with typically 1-2 rest days between.
An upper lower split would consist of training your upper body on day 1, and day 2 lower body, day 3 rest (repeat).

| Split type | Traing program | Description |
| :---       | :---        | :---        |
| Full Body  | [BSF Home beginner Full body](../training-programs/bsf-home-beginner-routine-full-body.md) | Our own budget friendly at home workout program for beginners. |
| Full Body  | [BSF Gym beginner Full body](../training-programs/bsf-gym-beginner-routine-full-body.md) | Beginner workout that focusses on barbell compounds. |
| Full Body  | [Scooby's beginner home routine](https://scoobysworkshop.com/beginning-workout-plan/) | This is a decent program if you have little equipment and just want to get started at home. Foccus is on general health and accessibility. I recommend getting some equipment and doing a better workout instead if you can.
| Upper+Lower | [Mike Israetel's upper lower](https://liftvault.com/programs/bodybuilding/mike-israetel-5-week-hypertrophy-workout-routine-spreadsheet/) |Good program for hypertrophy, as a beginner, read description on the site on how to use it. |
| Upper+Lower   | [PHUL upper lower program](https://docs.google.com/spreadsheets/d/1ojMF3NPDhOTW5pmrKPpNtnlyKU5z4UwwzrXtUE81R14/edit#gid=1933583211)|Good upper+lower (4day) split for gaining strength.|

### Create your own lifting routine

If you are a beginner (less than a year experience), don't  attempt to change an existing workout routine.
These routines are made by people who want you do grow optimally, don't cheat yourself out of your gains by editing them.
But you should want your independence from these existing programs after lifting for some time. Look at the section, [create your own routine](resistance-training/create-routine.md), if you are interested how to make one.

# FAQ

## I don't have equipment, should I do bodyweight/calisthenics?

> Calisthenics is a type of workout that uses a person’s body weight with little or no equipment.

Bodyweight, or calisthenics, won't get you the same results as classic bodybuilding. You want to have the ability to overload exercises without too much focus on complex technique. But If you want to do calisthenics because you think it's fun, give it a try.

We recommend to get at least a dumbbell set (of 2x12 or more) and a pull up bar to allow for more excersices that are designed for bodybuilding specifically.

## If I stop training for a week because of injury, school work etc, do I lose all my gains?

Losing muscles takes weeks/months of crappy dieting and no training to lose muscle mass, so don't worry too much if you can't make it to the gym.
If you skipped the gym be sure to keep up the diet plan, just because you're not training doesn't mean you can eat a lot different. If you skipped a workout (or ate something you might think it's *bad*) don't be upset. Just try not to miss the next workout. 

Doing something is infinitely  better than nothing, even if you do less than half of your normal volume or intensity, and if you only stick to half your diet plan. Even if you can only do some push ups that day, make the most out of it.

## Female training

Almost all information here will apply to you. The little bit of muscle you will put on by training properly will look good on you, you will not look bulky. These are some minor differences in diet and training listed [here](TODO) (link is broken).

## I want abs and big biceps, how should I train them?

Don't bother traning abs if aesthetics is your goal. DO NOT only train show muscles!
The latest *THENX 10 minutes every day ab* workout won't give you a great physique quickly.
I get that you want abs but I promise you, you will want a more proportionate body later.
You are better off with a complete training program.

## I read this acticle that said the opposide of what you are saying... what now?

There is a lot of varying information on the internet. Some of it is false, and some of it is just not truly optimal. What is written on this wiki you can trust, as it comes from passionate people who have researched hours, and trained years to get to where they are.

Fitness influencers tend to spread some false information or exaggerated info in order to get people to buy their products to fall for these traps. This wiki is 100% trusted info, and is reviewed by others to make sure it is all sound knowledge. 

We are not trying to sell you anything, we just want you to get good results.

## I don't want to get too big, can I put in a little effort to look a bit better?

NO! Most people put in too little effort in the gym!

Some research suggests that if you leave a couple of reps in reserve you can get decent results, this is called RIR training. So the answer is yes you can look decent with a little less effort. But RIR training means you still have to train hard! As a beginner just make sure you increase intensity each session slowly. Reps in reserve is not for beginners.

As a beginner maximum typically isn't needed to grow, but it becomes more important the longer you train.
Just have fun with the gym as beginner, but work hard, if you train half assed all the time for too long you will be wasting time in the gym.

## What are some gym etiquettes? I want to do good on my first day.

It depends on your gym but some general rules are:
* Put back the weights you have used.
* Don't interupt people who are in the middle of a set
* Clean up your bodily fluids
* Don't do supersets with every machine, especially if there's only one, instead try to vary by using the same cable machine or the same weight dumbbells hitting different muscle groups at a time
*Don't follow every single info at the gym. If you don't find something trustworthy just try to see if it's explained here!