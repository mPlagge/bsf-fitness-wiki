## Cardio

Cardio is a modality of exercise that will provide many health benefits, improve endurance performance, and can be used as a tool to aid fat loss.

What are the benefits to cardio?

Doing cardio will make you fitter and healthier by lowering your heart rate, decrease blood pressure, increase in stroke volume (amount of blood pumped with each beat), and help prevent obesity.

If the goal from your cardio is to improve your general health, then you should work on getting 150 minutes of moderate intensity  or 75 minutes of vigorous intensity a week, split however you like. This should be done in any way that you enjoy and keeps you motivated. However, if your goal is performance orientated, you should priotise your cardio to be the one you want to compete/perform in.

Different intensities can be used for different goals;

- If you want to improve general fitness/fat burning then your working intensity should be 40-60% of HRR (Heart rate reserve)
- If you want to improve aerobic fitness, your working intensity should be 60-80% of your HRR
- If improving your anaerobic fitness you want to work at an intensity of 80%+ of your HRR
- An intensity above 90% will be working on your VO2 max

How do I calculate heart rate reserve?

You need to first calculate your max heart rate with: 207 - 0.7 x age. Once you have this, subtract your resting heart rate from this number, this is your heart rate reserve. After you have this number, calculate the percentage based on your intensity goal i.e. HRR x 0.6 for 60%, and add it to your resting heart rate. You now have your target HR for your session. 



