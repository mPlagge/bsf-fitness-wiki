# Creatine supplementation

## What is creatine? 
Creatine is a natural occurring non-protein compound that our body uses for energy production in the form of ATP-CP. 

Creatine essentially allows us to performance more efficiently in the gym. We will be slightly stronger, and may be able to performa slightly more reps. Its not like a steroid it won't make you get insanely big all of a sudden, although over time it can contribute to more gains. 

Its role is to convert ADP molecules into ATP, which are energy our body uses during intense exercise such as lifting weight. Therefore creatine will also help with other sports you may be doing. 

Creatine is digested and creatine phosphate is delivered to different tissues in the body such as skeletal muscle tissue (muscles), brain tissues, and cardiac muscle (heart). 

There is actually quite a lot of creatine in fish and red meat however, when we cook it the creatine is broken down. Therefore we supplement it. 

## What creatine should I purchase? 
Creatine monohydrate

## Loading creatine
Creatine can be loaded 20g for 6-7 days should be a suffient loading period. DO NOT exceed this loading length. 
* TIP if you're going to load creatine for 6-7 days. Take 5g at a time 4 times a day, otherwise you could feel very sick, or get gastrointestinal upset. 

After your loading period you should continue taking creatine 3-5g a day. There's no point in cycling on and off of it.

* You can also just start off by supplementing with 3-5g and wait for a month or so and your creatine stores should be replenished. However, if you want to feel the benefits more immeditately the loading phase will be more desired.

## Creatine myths
1. Causes hairloss
2. Causes kidney, liver and heart problems (may do if you have underlying health problems)
3. Cycling on and off of it

## How to improve or optimise your creatine supplmentation?

Take it with carbs and proteins to get a more selective uptake into skeletal tissue rather than the other isoforms creatine phosphate comes in. 

