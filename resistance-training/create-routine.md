# Create your own routine

## Basics on hypertrophy

* Keep consistent, without consistency you won't grow.
* Train hard, give it your all don't take it easy.
* Listen to your body, this is an extremely powerful tool your body will tell you everything. 
* If you're far too tired to train maybe think about skipping that day as your intensity will be down.
* Progressive overload your lifts, increase volume out put. (sets, reps and weight). Personally I would recommend increasing weight and reps primarily on each of your exercises.
* Variation of training. Do top sets and back off sets. I.e set 1 heavy, set 2 medium to light, set 3 medium to light. Progressively overload the top sets and back off sets week in week out. You will grow!

## Excersices

You will need to select excersices for you routine in the [Excersice library](../resistance-training/excersice-library.md).

## Reps

* Rep ranges, work a range of reptitions. Best to do this on compounds work a lower rep range say 5-8, medium to higer rep range say 10-20. However on isolation style exercises it is best to stick to the medium to high rep range as going heavy on these won't really attribute to anything. 

* RIR(reps in reserve), to and beyond failure training. Can be useful to have a few reps left in the tank like on compounds where chance of injury and fuck ups are higher. However, most of the time on lower risk exercises you should be going to failure or beyond it with: cheat reps, partials to exhaust the lengthened range, and forced reps. Use of intensity techniques such as drop sets, or cluster sets can be also manipulated to increase workout intensity. 

## Sets

* Drop sets- drop sets are similar to cluster sets they're are a intensity technique used to push beyond failure prmarily for intermediate/advanced lifters as this will challenge the worked muscle a bit. Pick a weight do a set with it to failure, rest 10-20 seconds, decrease the weight slightly, do it to failure and repeat 2 more times. 

* Cluster sets- pick a weight you can perform about 10 reps on, leave a few reps in the tank. Give yourself 10-20 second rest, go again till failure, and repeat this 2 more times. Only recommend doing these on isolation/safe exercises can be used if you have a spotter. Helps increase volume, per set. I.e before you got to failure with 13 reps, but have minisets inside the cluster set to increase overall set volume (giving more volume overall). Also do these as a finisher. 

* Super sets drop sets when to use them? When you want to increase intensity of a set. (Advanced technique)
* How many sets per muscle group? Anything from 8 sets a week up to 20 sets a week is a good guideline, just change your sets depending on how you're recovering from your sessions. 

## Training splits

* What Schedule do you have? A busier schedule would benefit from full body routines, if you can only go to the gym 2-3x a week. If you're able to go to the gym everyday opt for a PPL, however this is more an intermediate/advanced split. Beginners should stick to Upper lower or Full body. 

* What is your recovery time? If you can recover quickly on full body for examle you can add more a 4th full body day, to train your whole body x4 a week. If you can recovery fast then you also may not be working hard enough in the session so look to changing your intensity, and volume. Maybe add a couple more sets in somewhere or adding a new exercise with like 2 sets, but with logical reason. If you're stuck on this tag @Leigh in BSF fitness server. 

### Bro split

* Typically not useful unless you're an extremely advanced trainee, and need a ton of volume to elecit growth on a particular muscle (beginners stay away from this, even intermediates and early advanced.)

### PPL

* Typically a great split if you're an intermediate/advanced. Gives you a good amount of frequency hitting all muscles 2x a week, and with a pretty high amount of volume. 
Push, pull, legs, push, pull, legs rest.

### Full body

* Awesome for beginners. Training the full body x3 a week, which will be optimal for the majority of beginners. A low amount of workout volume per muscle group which is why it is aimed at beginners as there is typically less volume needed for adaptations. (Highly recommend for beginners)
Day 1: full body, Day 2: rest, Day 3: rest, Day 4: full body, Day 5: rest, Day 6: full body, Day 7: rest.

### Upper lower

* Awesome for beginners and early intermediate. Training all muscles x2 a week, medium amount of volume per workout, so this is a good transition after full body. 
Day 1: Upper, Day 2: Lower, Day 3: rest, Day 4: Upper, Day 5: Lower, Day 6: rest. 

### Push pull

## From examples you will learn the most

You will probably learn the quickest by looking at existing routines. In the table listed down below we show a variety of training programs.

| Program | Description |
|---|---|
|[Leight's PPL](../training-programs/Leigh's_workout.md)|Leigh's PPL split, staf member|
|[Clemens' PPL](../training-programs/clemens_routine.md)|WIP|

# More info

* [Hypertrophy made simple](https://www.youtube.com/watch?v=4cW0EmO12Lk&list=PLyqKj7LwU2RukxJbBHi9BtEuYYKm9UqQQ&index=1)