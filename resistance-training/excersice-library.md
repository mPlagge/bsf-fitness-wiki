# Excersice Library

## Intoduction to exercise selection

### What muscles should I train?

You *NEED* to train your entire body. If you want to start editing or create your own training program it is essential to create all major muscle groups. Do not only train your chest biceps and abs.
It is said in the beginners guide and will be said again here. You don't want to look like a curlbro.
It simply looks horrible there should never be an excuse to neglect major body parts.

That being said, some muscles like calves, neck and abs might not need training. Since they get enough avtication from  compounds to proportionally grow with the rest of your body. But some people prefer to train them anyways for sport specific purposes or aesthetics. It all depends on your goals and beliefs.

### Train your Compounds.

Compounds training for big muscle groups like deadlifts, squats, chin ups should be the staple of your workout routine. Compounds hit multiple muscle groups at once, unlike isolation exercises. This will untimatly same you time in the gym, train muscles you can not target well otherwise and helps to keep your routine simple.
Isolation exercises do have their place, but compounds should come first.

### Free weights, not machines

Machines are a lot easier and might have a place if you have an injury or want to target a specific muscle.
But again, training the basic compounds with free weights should be your first choice.
Free weights like barbell training is a lot more difficult, a lot heavier, but will give you better results.

## Leg Training

### Training your quads

To train your quads effectively you need to pick an exercise with a good amount of knee extension, as this is the role of the quads. 

|Exercise|Description
|---|---|
|Squat | great overloading exercise. Keep chest out, push up through ur back, brace your whole midsection.|
|Hacksquat | great exercise for overloading the quads without using too much spinal load. | 
|Pendulum squat | TODO |
| Leg press |on a leg press machine to activate the quads the most you should make sure your feet are placed at the bottom of the platform and a quite narrow stance that feels comfortable. Go for full ROM, it is fine to lockout the knee as long as if you're not jerking the weight up and down keep it slow and controlled. |
|Leg extension | an essential exercise if you want to isolate the quads, ensure full rom. Controlled eccentrics and full concentrics in a controlled manner to prevent knee injuries. |

### Training your hamstrings

To train your hamstrings effectively you need to pick an exercise with lots of knee flexion, as this is the role of the hamstrings. 

|Exercise|Description
|---|---|
|Seated leg curl | Perfect exercise for isolating, and overloading the hamstrings. Keep the pad against the quad securely, and make sure the rotation of axis on the machine is lined with your knee. Hold your hands onto the machine, and dig yourself in. Focus on controlled eccentrics, this will fuck your hamstrings up big time.|
|RDL/dumbbell RDL|Awesome exercise for overloading the hamstrings, stick your butt out, and keep braced, along with a neutral spine. Would place these after attacking the hamstrings with a leg curl| 

### Training your calves

Similarly to abs, to train or not training calves is a controversial opinion. You should decide for yourself if you wan to incorporate calve training in your routine.

TODO Table

# Training your chest

## What a chest program should look like

* a Main press TODO
* an Incline TODO

## Excersices

The chest gets a stronger contraction the closer the elbows get together, and a stretch the further the elbows get apart. 

|Exercise|Description
|---|---|
|Flat Bench press |Awesome exercise for overloading the chest, put this first in your routine to put most of the intensity into it as it typically tends to be a strong and energy demanding movement.|
|Incline dumbbell/incline bench press|Incline presses are amazing for hitting the upper portion of the pecs a bit more which people tend to lack, put this second in your routine after a flat press or even before if you want to priortise upper chest growth.| 
|Chest pressing machine flat and incline|If your gym has a prime machine these are possibly something you would want to use as they give you optional loading points to suit the pectoral muscle where it is strongest or weakest (strongest in the middle of the motion) so you would want to load at the middle pin, and change the loading points if you want to fatigue different ranges. Overall machines are great ways to add volume which don't expend too much energy while giving you huge chest activation/distruption.| 
|Chest flys|Last thing in your chest routine would be a fly exercise after flat and incline pressing motions. Pec deck or seated cable flys would be optimal, and you should focus on getting the pecs very short (bringing the elbows or biceps together, and a full stretch on the eccentric in a nice controlled manner.) 

## Barbell training
TODO

## Squats

### I can't squat deep enough

You can try to put a plate under your heels, but long term you need to work on your mobility.
Usually it's coused by stiff calves.

* https://www.youtube.com/watch?v=J_ekvFybels&t=820s&ab_channel=CalgaryBarbell
* https://www.youtube.com/watch?v=vaMYBmJH-tw&t=781s&ab_channel=SquatUniversity

# Resistance profiles

Something to consider when picking an exercise is the resistance profile of an exercise. This essentially means picknig the right exercise for the goal of growing a particular muscle. 

Ideally a perfect exercise would have heaviest torche on the muscle when it is strongest. A prime example of this would be in a reverse banded leg press. The longer the band gets the higher tension it creates so as you're pushing up on the concentric of a leg press there is increasing torche, this is where the quad is strongest. However at the bottom where the knee is flexed this is where the quad is weakest, and at the point there is huge slack on the band therefore there is little torche, and this allows you to overload the shortened/mid range of the movement (where the muscle is strongest) and fatigue this part at a similar time the lengthened part would fatigue. 

# Vidoes on resistance profiles
https://www.youtube.com/watch?v=faGwi4nsjQc&ab_channel=BenPakulski-MuscleIntelligence

https://www.youtube.com/watch?v=4BmrRRTXra8&ab_channel=PersonalTrainingdotcom

