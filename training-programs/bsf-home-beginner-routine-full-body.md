# BSF' home starting routine

This routine is for anyone who wants to take fitness/bodybuilding serious but doesn't want to, or can't go to the gym.
An at home training program should get you a long way, but a gym might be more optimal if you want to grow faster.
But an at home training program is less expensive, more accessible and saves you a lot of time.
And if you invest in more expensive equipment in the long run, it can be as good as a professional gym.

You will need some equipment to make training more effective and more fun. In the section, [home gym equipment](resistance-training/buying_equipment.md), you can find the equipment you need for your home starter gym.

## Intensity

As a beginner, the most important thing is that you don't get injured by going all out on day one. Take things slow and you will need to add more Intensity eatch session. Make sure your reps and weight are going up.

Note that this workout can get instense very fast if you push hard on the sets. After a while you should be pushing every excercise as hard as possible.

## What do sets and reps mean (+how much rest in between)?

* Reps is short for repetitions, or the number of times in a row without pause.
* Sets in a workout are how many times you will repeat a the given rep number of a given exercise.

Your rest between sets should be 90 seconds to two minutes. Set a timer to make sure you don't go too early or too late. If you still feel exhausted after two minutes you could take more rest, but it should not be necessary, just keep going. 

Keep your rep ranges from 6-12. If you can do more than 12 reps. Increase the weight a bit. If your weights are too light to do overload the excercise, consider getting heavier dumbells (you can also buy single plates, which is cheaper). If you don't feel like upgrading your dumbells, going for 15+ reps is a possiblity but it's not optimal for your program.

# The training program

| Training   | M | T | W | T | F | S | S |
|---         |---|---|---|---|---|---|---|
|Bodybuilding|A  |   |B  |   |C  |   |   |
|Cardio      |X  |X  |X  |X  |X  |X  |X  |

## Cardio

Chose one cardio session you might like. Eatch of these rows represents a single cardio session.

|Cardio session|Time (minutes)|
|---|---|
|Bike|20|
|Running|20|
|Walking (maybe walk the dog?)|40|

## Excercises explained

The workout just has 6 excercises to keep things simple. They are the same for every session just in a different order. This is because you can push the hardest on the first few excersices you do. This way you will his every muscle group the hardest once per week.

### DB Weighted Push up

You might not be able to do a single push up. That's fine. I started in that situtuation as well. The good news is that you can do them on your knees. Just keep your back straight for both a knee or a normal push up. If it gets too easy, overload the excercise with dumbells on your back. If they slip off you can put them in a backpack.

### DB Shoulder press

TODO

[Dumbbell Press For Shoulders](https://www.youtube.com/watch?v=4RzcUV0qdV0)

### Chin up

You may also find that chin ups will be very difficult. Don't worry watch [How To Do Your First Pullup! (Then 8 more!)](https://www.youtube.com/watch?v=mRznU6pzez0) to see how to get started on chin ups/pull ups. Note that the video's uses different rep and set ranges. But the excersices themselfs should be a good progression to 6 solid chin ups.

Just strugle any of the progressions, combinations of them, whatever, untill you have at least a good 8 reps. Be sure to make it more difficult each week.

### DB Row

This is a back excercise that will ultimately also work your biceps. Try to lift as much as possible with your back. You can do this by thinking to pull your elbow up instead of the dumbell. 
[How To: Dumbbell Bent-Over Row (Single-Arm)](https://www.youtube.com/watch?v=pYcpY20QaE8)

### DB Lunge

You can do forward stepping lunges, or backward stepping lunges. It is a persomal preference. This video explains the basics of the excercise: [How To: Dumbbell Stepping Lunge](https://www.youtube.com/watch?v=D7KaRcUTQeE).

If you hurt your knees on the ground on the way down I recommened putting down a pillow. But it is better to not hit the ground at all.

### DB SLDL

TODO

[Stiff Leg Hamstring Deadlift With Dumbbells](https://www.youtube.com/watch?v=_5DEdWpKYj4)
## A, Push -> Pull --> legs

| Lift | Reps | Sets |
|------|------|------|
|DB Weighted Push Up |6-12|3|
|DB Shoulder press|6-12|3|
|Chin up|6-12|3|
|DB Row|6-12|3|
|DB Lunge|6-12|3|
|DB SLDL|6-12|3|

## B, Pull--> Legs--> Push
| Lift |  Reps | Sets |
|------|--------|------|
|Chin up|6-12|3|
|DB Row|6-12|3|
|DB Lunge|6-12|3|
|DB SLDL|6-12|3|
|DB Weighted Push Up |6-12|3|
|DB Shoulder press|6-12|3|

## C, Legs --> Push --> Pull

| Lift |  Reps | Sets |
|------|--------|------|
|DB Lunge|6-12|3|
|DB SLDL|6-12|3|
|DB Weighted Push Up |6-12|3|
|DB Shoulder press|6-12|3|
|Chin up|6-12|3|
|DB Row|6-12|3|