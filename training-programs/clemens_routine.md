# Clemens' new workout, WIP

ABC rest repeat. Example in table below:

|M  |T  |W  |T  |F  |S  |S  |M  |...|
|---|---|---|---|---|---|---|---|---|
|A  |B  |C  |   |A  |B  |C  |   |...|

# Wish list

* More plates
* Bench and rack
* Do olypic squat clean when form get better.

# Rest

Arround 2 minutes. Usually a bit more. If I need more I take more.

# Intensity

Everything as close to failure as possible.
Do whatever needed to get the reps up from the last session.

# A, PUSH
|LIFT|SETS|REPS|NOTES|
|---|---|---|---|
|Weighted Push up|3|6-12|I want to replace this with bench press|
|DB chest fly, floor|3|6-12||
|BB OPH|3|6-12|Replace with seated dumbell press for now|
|DB Shoulder fly, seated|3|6-12||
|BB Skullcrushers|3|6-12+||

# B, PULL
|LIFT|SETS|REPS|NOTES|
|---|---|---|---|
|Pull up|3|6-12||
|BB Pedlay row|3|6-12||
|Chin up|3|6-12|Might take this out for now|
|BB Curl|3|6-12||
|DB Curl, hammer|3|6-12||

# C, LEGS

|LIFT|SETS|REPS|NOTES|
|---|---|---|---|
|Pl8 Lunges|3|12-24|Hold on to plate handles, kind of like db lunges. If one plate is too light bind them together with belts. Want to replace this with squats when getting a rack.|
|BB SLDL|3|6-12||
|PL8 belt, single leg, calve raises|3|6-12|Bind plates on hip with a belt. Hold on to something stable and do calve raises.|